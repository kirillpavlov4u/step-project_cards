# Step project "CARDS"

## Overview
This is the webpage of step project "CARDS".
Made by DAN.IT students of the "Front-End" Course.

### To start using the product:
    1. Click the "Sign in" button, fill the e-mail and password fields:
        - e-mail: av_zayats@ukr.net
        - password: cards2020

    2. To create a new card, click the "Create a visit" button.
    3. To edit a card, click the "Edit" button of the particular card.
    4. To delete a card, click the "X" button of the particular card and fill the password.
    5. To filter added cards by keywords, specialist, status and priority, use filters block.
       
### Participants of the project:
    1. Pavlov Kyrylo,
    2. Zayats Oleksandr,
    3. Zayats Roman

### Tasks of the participants:

    - Gulpfile.js - made by O.Z., R.Z., K.P.
    - JS Modules implementation - K.P.
    - Babel Complier - K.P.
    - Styling - made by K.P., O.Z.
    - Class "Cards" - made by O.Z.
    - Classes "Modal", "Inputs", "Form" - made by K.P.
    - Class "Filter" - made by R.Z.
    - Drag/Drop - made by O.Z.