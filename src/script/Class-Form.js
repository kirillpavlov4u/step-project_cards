import * as inputs from "./Class-Inputs.js";
import { Card } from "./Class-Card.js";

class Form {
  constructor(id) {
    this.element = document.createElement("FORM");
    this.id = id;
    this.modalContainer = document.querySelector(".modal-container");
  }

  compareInitialAndEditedCards (initial, edited) {
    let initialCard = initial;
    delete initialCard.id;
    initialCard = JSON.stringify(initialCard);
    const editedCard = JSON.stringify(edited);

    if (initialCard !== editedCard) {
      return true;
    }
  }

  collectFormData(formId, initialCard, cardId) {
    const formElements = [...document.forms[formId].elements];
    const formData = {}
    
    formElements.forEach(elem => {
      if (elem.type === "submit" || elem.type === "button") {
        return;
      }

      formData[elem.name] = elem.value;
    });

    if (formData.password) {
      this.sendAuthRequest(formData);
    } else if (formId === "editVisitForm") {
      if(this.compareInitialAndEditedCards(initialCard, formData)) {
        this.sendPutRequest(formData, cardId)
      } else {
        alert('Add some changes to the Visit card,\nor close the Edit window!')
        return;
      }
    } else if (formId === "createNewVisit") {
      this.sendPostRequest(formData);
    }  
  }

  sendPutRequest(jsonData, cardId) {
    fetch(`http://cards.danit.com.ua/cards/${cardId}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem("auth")).token}`
      },
      method: "PUT",
      body: JSON.stringify(jsonData)
    })
    .then(res => res.json())
    .then(d => {
      if (d.status === "Error") {
        alert("Something went wrong while updating a card\nPlease, try again");
      } else {
        alert(`Card under id${cardId} was successfuly updated`);
        document.querySelector("#visits-filter").submit();
        this.closeModal();
      }  
    });
  }

  sendAuthRequest(jsonData) {
    fetch("http://cards.danit.com.ua/login", {
      method: "POST",
      body: JSON.stringify(jsonData)
    })
    .then(r => r.json())
    .then(d => {
      if (d.status === "Success") {
        localStorage.setItem(
          'auth',
          JSON.stringify(
            {
              token: d.token,
              password: jsonData.password,
              email: jsonData.email
            }
          )
        );
				alert("You are signed in now.");
        document.querySelector("#visits-filter").submit();
        document.querySelector(".button-sign-in").hidden = true;
        this.closeModal();
      } else {
        alert(`${d.status}: ${d.message}`);
      }
    });
  }

  sendPostRequest(jsonData) {
    fetch("http://cards.danit.com.ua/cards", {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem("auth")).token}`
      },
      method: "POST",
      body: JSON.stringify(jsonData)
    })
    .then(r => r.json())
    .then(d => {
      if (d.status === "Error") {
        alert("First sign in and then create a card");
      } else {
        new Card("visits-table", d).renderCard();
        this.closeModal();
      }
    });
  }

  removeNextSibling(referenceElem) {
    if (referenceElem.nextElementSibling.type !== "submit") {
      referenceElem.nextElementSibling.remove();
      this.removeNextSibling(referenceElem);
    }  
  }

  addElements(elements) {
    const fragment = document.createDocumentFragment();

    elements.forEach(elem => {
      fragment.append(elem);
    })

    return fragment;
  }

  getElement(elements = []) {
    this.element.id = this.id;

    if (elements.length === 0) {
      alert("Add form elements to the custom form");
    } else {
      this.element.append(this.addElements(elements));
    }

    return this.element;
  }

  closeModal() {
    const modal = document.querySelector(".modal-container");
    modal.innerHTML = "";
    modal.hidden = true;
  }
}

export class AuthForm extends Form {
  constructor(id = "authForm") {
    super(id);
    this.elements = [
      new inputs.EmailInput(this.id).getElement(),
      new inputs.PasswordInput(this.id).getElement(),
      new inputs.Submit(this.id).getElement("Вход")
    ]
  }

  onSubmitClick(event) {
    event.preventDefault();
    if(this.element.checkValidity() && this.element.reportValidity()) {
      Form.prototype.collectFormData(this.id);
    } else {
      this.element.checkValidity();
      this.element.reportValidity();
    }
  }

  getElement() {
    this.element.id = this.id;

    const submitListener = this.elements.filter(elem => elem.classList.contains("form__submit"));
    submitListener[0].addEventListener("click", () => this.onSubmitClick(event));
    
    this.element.append(this.addElements(this.elements));

    return this.element;
  }
}

export class CreateVisitForm extends Form {
  constructor(id = "createNewVisit") {
    super(id);
    this.elements = [
      new inputs.SelectSpecialist(this.id).getElement("specialist"),
      new inputs.Submit(this.id).getElement("Создать"),
      new inputs.CloseButton(this.id).getElement()
    ]
  }

  submitButton(event) {
    event.preventDefault();

    if (document.querySelector(".select-input").value == "none") {
      alert("You must first select specialist");
      return;
    }

    if(this.element.checkValidity() && this.element.reportValidity()) {
      Form.prototype.collectFormData(this.id);
    } else {
      this.element.checkValidity();
      this.element.reportValidity();
    }
  }

  closeButton(event) {
    this.elements[2].removeEventListener("click", () => this.closeButton(event));
    this.modalContainer.innerHTML = "";
    this.modalContainer.hidden = true;
  }

  onSelectChange(event) {
    Form.prototype.removeNextSibling(this.elements[0]);

    if (event.target.value == "cardiologist") {
      this.elements[0].insertAdjacentElement(
        "afterend",
        new VisitCardiologist(this.id).getElement()
      );
    }

    if (event.target.value == "dentist") {
      this.elements[0].insertAdjacentElement(
        "afterend",
        new VisitDentist(this.id).getElement()
      );
    }

    if (event.target.value == "therapist") {
      this.elements[0].insertAdjacentElement(
        "afterend", 
        new VisitTherapist(this.id).getElement()
      );
    }
  }

  getElement() {
    this.element.id = this.id;

    const selectListener = this.elements.filter(elem => {
      if (elem.classList) {
        return elem.classList.contains("select-input")
      }
    });

    selectListener[0].addEventListener("change", () => this.onSelectChange(event));
    this.elements[1].addEventListener("click", () => this.submitButton(event));
    this.elements[2].addEventListener("click", () => this.closeButton(event));

    this.element.append(this.addElements(this.elements));

    return this.element;
  }
}

export class EditVisitForm extends Form {
  constructor(id = "editVisitForm") {
    super(id);
    this.elements = [
      new inputs.SelectSpecialist(this.id).getElement("specialist"),
      new inputs.Submit(this.id).getElement("Сохранить"),
      new inputs.CloseButton(this.id).getElement()
    ]
  }

  submitButton(event) {
    event.preventDefault();

    if (document.querySelector(".select-input").value == "none") {
      alert("You must first select specialist");
      return;
    }

    if(this.element.checkValidity() && this.element.reportValidity()) {
      Form.prototype.collectFormData(this.id, this.cardData, this.cardData.id);
    } else {
      this.element.checkValidity();
      this.element.reportValidity();
    }
  }

  closeButton(event) {
    this.elements[2].removeEventListener("click", () => this.closeButton(event));
    this.modalContainer.innerHTML = "";
    this.modalContainer.hidden = true;
  }

  onSelectChange(event) {
    Form.prototype.removeNextSibling(this.elements[0]);
    const form = this.setInputs(event.target.value);

    this.elements[0].insertAdjacentElement(
      "afterend",
      form
    );
  }

  setInputs(specialist) {
    let form;

    if (specialist === "cardiologist") {
      form = new VisitCardiologist(this.id).getElement();
    } else if (specialist === "dentist") {
      form = new VisitDentist(this.id).getElement()
    } else if (specialist === "therapist") {
      form = new VisitTherapist(this.id).getElement()
    }

    const formInputs = [...form.children];

    if (this.cardData) {
      form.innerHTML = "";

      formInputs.forEach(item => {
        item.value = this.cardData[item.name];
        form.append(item);
      });
    }

    return form;
  } 

  setDefaultSelect(specialist) {
    Form.prototype.removeNextSibling(this.elements[0]);
    this.defaultSelectedOption(specialist);
    const form = this.setInputs(specialist, this.cardData);

    this.elements[0].insertAdjacentElement(
      "afterend",
      form
    );
  }

  defaultSelectedOption(specialist) {
    this.elements[0].value = specialist;
  }

  getElement(dataObject) {
    this.element.id = this.id;
    this.cardData = dataObject;
    

    const selectListener = this.elements.filter(elem => {
      if (elem.classList) {
        return elem.classList.contains("select-input")
      }
    });

    selectListener[0].addEventListener("change", () => this.onSelectChange(event));
    this.elements[1].addEventListener("click", () => this.submitButton(event));
    this.elements[2].addEventListener("click", () => this.closeButton(event));

    this.element.append(this.addElements(this.elements));

    if (this.cardData) {
      this.setDefaultSelect(this.cardData.specialist);
    }
    
    return this.element;
  }
}

export class Visit {
  constructor(id) {
    this.id = id;
    this.elements = document.createDocumentFragment();
  }

  getElement() {
    this.elements.append(
      new inputs.SelectStatus(this.id).getElement("status"),
      new inputs.AppointmentGoalInput(this.id).getElement(),
      new inputs.Description(this.id).getElement(),
      new inputs.SelectPriority(this.id).getElement("priority")
    );

    return this.elements;
  }
}

export class VisitCardiologist {
  constructor(id) {
    this.id = id;
    this.elements = document.createElement("LABEL");
  }

  getElement() {
    this.elements.append(
      new Visit(this.id).getElement(),
      new inputs.BloodPressure(this.id).getElement(),
      new inputs.MassIndex(this.id).getElement(),
      new inputs.CardioDiseases(this.id).getElement(),
      new inputs.AgeInput(this.id).getElement(),
      new inputs.FullNameInput(this.id).getElement()
    );

    return this.elements;
  }
}

export class VisitDentist {
  constructor(id) {
    this.id = id;
    this.elements = document.createElement("LABEL");
  }

  getElement() {
    this.elements.append(
      new Visit(this.id).getElement(),
      new inputs.LastVisitInput(this.id).getElement(),
      new inputs.FullNameInput(this.id).getElement()
    );

    return this.elements;
  }
}

export class VisitTherapist {
  constructor(id) {
    this.id = id;
    this.elements = document.createElement("LABEL");
  }

  getElement() {
    this.elements.append(
      new Visit(this.id).getElement(),
      new inputs.AgeInput(this.id).getElement(),
      new inputs.FullNameInput(this.id).getElement()
    );

    return this.elements;
  }
}