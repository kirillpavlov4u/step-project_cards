export class Modal {
  constructor(form) {
    this.container = document.querySelector(".modal-container");
    this.form = form;
  }

  clearContainer() {
    this.container.innerHTML = "";
  }

  clickOutsideTheForm(event) {
    if (event.target == this.form.parentNode) {
      document.removeEventListener("click", () => this.clickOutsideTheForm(event));
      this.clearContainer();
      this.container.hidden = true;
    }
  }

  getElement() {
    this.clearContainer();
    this.container.hidden = false;
    document.addEventListener("click", () => this.clickOutsideTheForm(event));

    this.container.append(this.form)
  }
}

