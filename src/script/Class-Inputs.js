export class Input {
  constructor (form) {
    this.element = document.createElement("INPUT");
    this.form = form;
  }

  getElement(name, placeholder, type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = placeholder;
    this.element.required = true;
    
    return this.element;
  }
}

export class EmailInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "email", type = "email", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your email";
    this.element.required = true;
    
    return this.element;
  }
}

export class PasswordInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "password", type = "password", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your password";
    this.element.required = true;
    
    return this.element;
  }
}

export class SearchInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "search", type = "search", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Search by title";
    
    return this.element;
  }
}

export class AppointmentGoalInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "purpose", type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Describe an appointment purpose";
    this.element.required = true;
    
    return this.element;
  }
}

export class BloodPressure extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "pressure", type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your usual blood pressure";
    this.element.required = true;
    
    return this.element;
  }
}

export class MassIndex extends Input {
  constructor(form) {
    super(form);
  }

  calculateBodyMassIndex(event) {
    const values = event.target.value.split(" ");

    if (values.length === 2) {
      event.target.value = (values[0] / Math.pow(values[1], 2)).toFixed(2);
    } else {
      alert("Enter your Weight(kg) and Height(m) e.g.: 78 1.86");
      event.target.value = "";
    }
  }

  getElement(name = "bmi", type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your Weight(kg) and Height(m) e.g.: 78 1.86";

    this.element.addEventListener('blur', () => this.calculateBodyMassIndex(event));
    
    return this.element;
  }
}

export class AgeInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "age", type = "number", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your age";
    this.element.required = true;
    this.element.min = "1";
    this.element.max = "130";
    
    return this.element;
  }
}

export class LastVisitInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "last-visit", type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your last visit date";
    
    return this.element;
  }
}

export class FullNameInput extends Input {
  constructor(form) {
    super(form);
  }

  getElement(name = "fullname", type = "text", value = "") {
    this.element.classList.add("form__input");
    this.element.type = type;
    this.element.name = name;
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    this.element.placeholder = "Enter your full name";
    this.element.required = true;
    
    return this.element;
  }
}

export class Submit extends Input {
  constructor(form) {
    super(form);
  }

  getElement(value = "") {
    this.element.classList.add("form__submit");
    this.element.type = "submit";
    this.element.setAttribute("value", value);
    this.element.setAttribute("form", this.form);
    
    return this.element;
  }
}

export class Select {
  constructor(form) {
    this.element = document.createElement("SELECT");
    this.form = form;
  }
  
  parseOptions(options) {
    const fragment = document.createDocumentFragment();

    options.forEach((option) => {
      const optionElem = document.createElement("OPTION");
      optionElem.classList.add("select-input__option");
      optionElem.setAttribute("value", option.optionValue);

      if (option.optionValue === "none") {
        optionElem.setAttribute("selected", true);
        optionElem.setAttribute("disabled", true);
        optionElem.setAttribute("hidden", true);
      }

      optionElem.textContent = option.optionText;

      fragment.append(optionElem);
    });

    return fragment;
  }

  getElement(name = "", options = this.options) {
    this.element.classList.add("select-input");
    this.element.name = name;
    this.element.setAttribute("form", this.form);
    this.element.required = true;

    this.element.append(this.parseOptions(options));
    
    return this.element;
  }
}

export class SelectStatus extends Select {
  constructor(form) {
    super(form);
    this.options = [
      {
        optionText: "Choose status",
        optionValue: "none"
      },
      {
        optionText: "Open",
        optionValue: "open"
      },
      {
        optionText: "Done",
        optionValue: "done"
      }
    ];
  }
}

export class SelectSpecialist extends Select {
  constructor(form) {
    super(form);
    this.options = [
      {
        optionText: "Select specialist",
        optionValue: "none"
      },
      {
        optionText: "Cardiologist",
        optionValue: "cardiologist"
      },
      {
        optionText: "Dentist",
        optionValue: "dentist"
      },
      {
        optionText: "Therapist",
        optionValue: "therapist"
      }
    ];
  }
}

export class SelectPriority extends Select {
  constructor(form) {
    super(form);
    this.options = [
      {
        optionText: "Select priority",
        optionValue: "none"
      },
      {
        optionText: "Low",
        optionValue: "low"
      },
      {
        optionText: "Normal",
        optionValue: "normal"
      },
      {
        optionText: "High",
        optionValue: "high"
      }
    ];
  }
}

export class TextArea {
  constructor(form) {
    this.element = document.createElement("TEXTAREA");
    this.form = form;
  }

  getElement(placeholder = "Enter your text here", name = "textarea") {
    this.element.classList.add("form__text-area");
    this.element.name = name;
    this.element.setAttribute("form", this.form);
    this.element.placeholder = placeholder;
    
    return this.element;
  }
}

export class Description extends TextArea {
  constructor(form) {
    super(form);
  }

  getElement(placeholder = "Enter a visit description", name = "description") {
    this.element.classList.add("form__text-area");
    this.element.name = name;
    this.element.setAttribute("form", this.form);
    this.element.placeholder = placeholder;
    this.element.required = true;
    
    return this.element;
  }
}

export class CardioDiseases extends TextArea {
  constructor(form) {
    super(form);
  }

  getElement(placeholder = "Describe occasions if you had cardio disiases", name = "cardio-diseases") {
    this.element.classList.add("form__text-area");
    this.element.name = name;
    this.element.setAttribute("form", this.form);
    this.element.placeholder = placeholder;
    
    return this.element;
  }
}

export class Button {
  constructor(form) {
    this.element = document.createElement("BUTTON");
    this.form = form;
  }

  getElement(addClass, text) {
    this.element.classList.add("form__button", `${addClass}-button`);
    this.element.textContent = text;
    this.element.type = "button";
    this.element.setAttribute("form", this.form);
    
    return this.element;
  }
}

export class CloseButton extends Button {
  constructor(form) {
    super(form);
  }

  getElement() {
    this.element.classList.add("form__button", `close-button`);
    this.element.textContent = "Закрыть";
    this.element.type = "button";
    this.element.setAttribute("form", this.form);
    
    return this.element;
  }
}

export class CreateVisitButton extends Button {
  constructor(form) {
    super(form);
  }

  getElement() {
    this.element.classList.add("form__button", `create-visit-button`);
    this.element.textContent = "Создать";
    this.element.type = "button";
    this.element.setAttribute("form", this.form);
    
    return this.element;
  }
}
