import { Card } from "./Class-Card.js";

export class Filter {
    constructor() {
        this.filterVisitsInput = document.getElementsByClassName("visits-filter__search")[0];
        this.filterVisitsStatus = document.getElementsByClassName("visits-filter__status")[0];
        this.filterVisitsPriority = document.getElementsByClassName("visits-filter__priority")[0];
        this.filterVisitsSpecialist = document.getElementsByClassName("visits-filter__specialist")[0];
        this.filterVisitsSubmit = document.getElementsByClassName("visits-filter__submit")[0];
        this.noItemsMsg = document.createElement("p");
    }

    windowOnload() {
      if (localStorage.getItem("auth")) {
        document.querySelector(".load-status").hidden = true;
        window.onload = () => {
        this.checkServerFetch()
          .then(allVisitsArr => {
              this.filteredArrCheckNRender(allVisitsArr)
          })
        }
      } else {
        document.querySelector(".load-status").hidden = false;
      }
    }

    filter(e) {
      e.preventDefault();
      [...document.getElementsByClassName("visits-item")].forEach(e => e.remove());

      this.checkServerFetch()
        .then(allVisitsArr => {
          if (this.filterVisitsInput.value) {
            const filteredVisitsArr = allVisitsArr.filter(visit => visit.purpose.toLowerCase().includes(this.filterVisitsInput.value.toLowerCase()) || visit.description.toLowerCase().includes(this.filterVisitsInput.value.toLowerCase()));
            this.filterVisitsInput.value = "";
            this.filteredArrCheckNRender(filteredVisitsArr);
          } else {
            if (this.filterVisitsStatus.value === "All" && this.filterVisitsPriority.value === "All" && this.filterVisitsSpecialist.value === "All") {
              const filteredVisitsArr = allVisitsArr;
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsStatus.value !== "All" && this.filterVisitsPriority.value !== "All" && this.filterVisitsSpecialist.value !== "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === this.filterVisitsStatus.value.toLowerCase() && visit.priority.toLowerCase() === this.filterVisitsPriority.value.toLowerCase() && visit.specialist.toLowerCase() === this.filterVisitsSpecialist.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsStatus.value === "All" && this.filterVisitsPriority.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.specialist.toLowerCase() === this.filterVisitsSpecialist.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsStatus.value === "All" && this.filterVisitsSpecialist.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.priority.toLowerCase() === this.filterVisitsPriority.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsPriority.value === "All" && this.filterVisitsSpecialist.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === this.filterVisitsStatus.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsStatus.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.priority.toLowerCase() === this.filterVisitsPriority.value.toLowerCase() && visit.specialist.toLowerCase() === this.filterVisitsSpecialist.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsPriority.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === this.filterVisitsStatus.value.toLowerCase() && visit.specialist.toLowerCase() === this.filterVisitsSpecialist.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }

            if (this.filterVisitsSpecialist.value === "All") {
              const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === this.filterVisitsStatus.value.toLowerCase() && visit.priority.toLowerCase() === this.filterVisitsPriority.value.toLowerCase());
              this.filteredArrCheckNRender(filteredVisitsArr);
            }
          }
        })
    }

    appendNoItemsMsg() {
      // this.noItemsMsg.classList.add("no-items-msg");
      // this.noItemsMsg.textContent = "No items added...";
      document.getElementById("visits-table").append(this.noItemsMsg);
    }

    checkServerFetch() {
        return fetch("http://cards.danit.com.ua/cards", {
            headers: {
                Authorization: `Bearer ${JSON.parse(localStorage.getItem("auth")).token}`
            },
            method: "GET",
        })
          .then(response => response.json())
    }

    filteredArrCheckNRender(arr) {
        if (arr.length === 0) {
          this.appendNoItemsMsg()
            // alert("No such items have been added");
            // STRING.RENDER HERE!
        } else {
            this.noItemsMsg.remove();

            arr.forEach((e)=>{
                const card = new Card('visits-table', e);
                card.renderCard();
                card.dragCard()
            })
        }
    }
}





















// const filterVisitsInput = document.getElementsByClassName("visits-filter__search")[0];
// const filterVisitsStatus = document.getElementsByClassName("visits-filter__status")[0];
// const filterVisitsPriority = document.getElementsByClassName("visits-filter__priority")[0];
// const filterVisitsSpecialist = document.getElementsByClassName("visits-filter__specialist")[0];
// const filterVisitsSubmit = document.getElementsByClassName("visits-filter__submit")[0];
//
// const checkServerFetch = () => {
//     return fetch("http://cards.danit.com.ua/cards", {
//         headers: {
//             Authorization: "Bearer 3b2ba5120f89"
//         },
//         method: "GET",
//     })
//       .then(response => response.json())
// }
//
// window.onload = () => {
//     // checkServer();
//     checkServerFetch()
//       .then(allVisitsArr => {
//           filteredArrCheckNRender(allVisitsArr)
//       })
// };
//
// let noItemsMsg = document.createElement("p");
// noItemsMsg.classList.add("no-items-msg");
// noItemsMsg.textContent = "No items added...";
//
// function filteredArrCheckNRender(arr) {
//
//     if (arr.length === 0) {
//         document.getElementById("visits-table").append(noItemsMsg);
//         // alert("No such items have been added");
//         // STRING.RENDER HERE!
//     } else {
//         noItemsMsg.remove();
//
//         arr.forEach((e)=>{
//            const card = new Card('visits-table', e);
//             card.renderCard();
//
//         })
//     }
// }
//
// filterVisitsSubmit.addEventListener("click", (e) => {
//     e.preventDefault();
//
//     [...document.getElementsByClassName("visits-item")].forEach(e => e.remove());
//
//     checkServerFetch()
//         .then(allVisitsArr => {
//             if (filterVisitsInput.value) {
//                 const filteredVisitsArr = allVisitsArr.filter(visit => visit.title.toLowerCase().includes(filterVisitsInput.value.toLowerCase()) || visit.description.toLowerCase().includes(filterVisitsInput.value.toLowerCase()));
//                 filterVisitsInput.value = "";
//                 filteredArrCheckNRender(filteredVisitsArr);
//             } else {
//                 if (filterVisitsStatus.value === "All" && filterVisitsPriority.value === "All" && filterVisitsSpecialist.value === "All") {
//                     const filteredVisitsArr = allVisitsArr;
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsStatus.value !== "All" && filterVisitsPriority.value !== "All" && filterVisitsSpecialist.value !== "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === filterVisitsStatus.value.toLowerCase() && visit.priority.toLowerCase() === filterVisitsPriority.value.toLowerCase() && visit.doctor.toLowerCase() === filterVisitsSpecialist.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsStatus.value === "All" && filterVisitsPriority.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.doctor.toLowerCase() === filterVisitsSpecialist.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsStatus.value === "All" && filterVisitsSpecialist.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.priority.toLowerCase() === filterVisitsPriority.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsPriority.value === "All" && filterVisitsSpecialist.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === filterVisitsStatus.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsStatus.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.priority.toLowerCase() === filterVisitsPriority.value.toLowerCase() && visit.doctor.toLowerCase() === filterVisitsSpecialist.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsPriority.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === filterVisitsStatus.value.toLowerCase() && visit.doctor.toLowerCase() === filterVisitsSpecialist.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 } else if (filterVisitsSpecialist.value === "All") {
//                     const filteredVisitsArr = allVisitsArr.filter(visit => visit.status.toLowerCase() === filterVisitsStatus.value.toLowerCase() && visit.priority.toLowerCase() === filterVisitsPriority.value.toLowerCase());
//                     filteredArrCheckNRender(filteredVisitsArr);
//                 }
//             }
//         })
// });
































// fetch("http://cards.danit.com.ua/cards", {
//   headers: {
//     Authorization: "Bearer 3b2ba5120f89"
//   },
//   method: "POST",
//   body: JSON.stringify({
//     title: "Visit to the dentist",
//     description: "Just visit",
//     doctor: "dentist",
//     status: "open",
//     priority: "low",
//     age: 34,
//     weight: 45
//   })
// });

// window.onload = () => {
//     if (localStorage.getItem("filteredVisitsArr")) {
//         const filteredVisitsArr = JSON.parse(localStorage.getItem("filteredVisitsArr"));
//         visitsArrCheckNRender(filteredVisitsArr);
//     } else {
//         refreshServer();
//     }
// };

// window.closed = () => {
//     localStorage.getItem("filteredVisitsArr").remove();
// };

// function checkServer() {
//     checkServerFetch()
//       .then(allVisitsArr => {
//           visitsArrCheckNRender(allVisitsArr)
//       })
// }