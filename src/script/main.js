import { Modal } from "./Class-Modal.js";
import { CreateVisitForm } from "./Class-Form.js";
import { AuthForm } from "./Class-Form.js";
import { Filter } from "./filter.js";

document.addEventListener("DOMContentLoaded", addSignInListener);

function addSignInListener () {
  const signInButton = document.querySelector(".button-sign-in");

  signInButton.addEventListener("click", () => {
    const newAuthForm = new AuthForm().getElement();

    new Modal(newAuthForm).getElement();
  });
}

document.addEventListener("DOMContentLoaded", addCreateVisitListener);

function addCreateVisitListener () {
  const screateVisitButton = document.querySelector(".button-create-visit");

  screateVisitButton.addEventListener("click", () => {
    if (!JSON.parse(localStorage.getItem("auth"))) {
      alert('You are log out\nPlease, Sign in first.');
      return;
    }
    const newVisitForm = new CreateVisitForm().getElement();

    new Modal(newVisitForm).getElement();
  });
}

document.addEventListener("DOMContentLoaded", () => {
  const newFilter = new Filter();
  newFilter.windowOnload();
  newFilter.filterVisitsSubmit.addEventListener("click", (e) => newFilter.filter(e));

  if (!JSON.parse(localStorage.getItem("auth"))) {
    document.querySelector(".button-sign-in").hidden = false;
  } else {
    document.querySelector(".button-sign-in").hidden = true;
  }
});



