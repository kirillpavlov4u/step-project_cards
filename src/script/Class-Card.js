import { Modal } from "./Class-Modal.js";
import { EditVisitForm } from "./Class-Form.js";

export class Card {
    constructor(containerSelector, infoObject) {
        this.password = JSON.parse(localStorage.getItem("auth")).password;
        this.infoObject = infoObject;
        this.elements = {
            container: document.getElementById(containerSelector),
            self: document.createElement('div'),
            draggableElement: document.createElement('div'),
            moreInfoBtn: document.createElement('button'),
            editCardBtn: document.createElement('button'),
            deleteCardBtn: document.createElement('button'),
            deleteCardForm: document.createElement('form'),
            deleteCardFormText: document.createElement('span'),
            deleteCardPass: document.createElement('input'),
            deleteCardSubmBtn: document.createElement('button'),
            deleteCardCancelBtn: document.createElement('button')
        }
    }

    renderCard() {
        let hiddenInfoElements = [];
        for (let key in this.infoObject) {
            if (this.infoObject[key]) {
                this[key] = this.infoObject[key];
                this.elements[key] = document.createElement('p');
                this.elements[key].innerText = `${key}:  ${this.infoObject[key]}`;
                this.elements.self.append(this.elements[key])
            }

        }
        for (let el in this.elements) {
            if (this.elements[el] !== this.elements.container && this.elements[el] !== this.elements.self && this.elements[el] !== this.elements.moreInfoBtn && this.elements[el] !== this.elements.editCardBtn && this.elements[el] !== this.elements.deleteCardBtn && this.elements[el] !== this.elements.deleteCardForm && this.elements[el] !== this.elements.deleteCardFormText && this.elements[el] !== this.elements.deleteCardPass && this.elements[el] !== this.elements.deleteCardSubmBtn && this.elements[el] !== this.elements.deleteCardCancelBtn && this.elements[el] !== this.elements.specialist && this.elements[el] !== this.elements.purpose&& this.elements[el] !== this.elements.draggableElement) {
                this.elements[el].classList.add('invisible');
                hiddenInfoElements.push(this.elements[el]);
            }
        }
        this.elements.draggableElement.classList.add('drag-element');
        this.elements.moreInfoBtn.innerText = 'Show more/less info';
        this.elements.editCardBtn.innerText = 'Edit';
        this.elements.deleteCardBtn.innerText = 'X';
        this.elements.deleteCardBtn.classList.add('visits-item__dlt-btn');
        this.elements.deleteCardSubmBtn.className = 'visits-item__dlt-conf-btn btn';
        this.elements.deleteCardCancelBtn.className = 'visits-item__dlt-cancel-btn btn';
        this.elements.deleteCardForm.classList.add('visits-item__dlt-form');
        this.elements.deleteCardFormText.innerText = 'Please enter password to delete card:';
        this.elements.deleteCardFormText.classList.add('visits-item__dlt-form-text');
        this.elements.deleteCardPass.classList.add('visits-item__dlt-pass');
        this.elements.moreInfoBtn.className = 'visits-item__info-btn btn';
        this.elements.editCardBtn.className = 'visits-item__edit-btn btn';
        this.elements.deleteCardPass.type = 'password';
        this.elements.deleteCardPass.placeholder = 'enter password';
        this.elements.deleteCardSubmBtn.innerText = 'Delete';
        this.elements.deleteCardCancelBtn.innerText = 'Cancel';
        this.elements.deleteCardForm.classList.add('invisible');
        this.elements.deleteCardForm.append(this.elements.deleteCardFormText, this.elements.deleteCardPass, this.elements.deleteCardSubmBtn, this.elements.deleteCardCancelBtn);


        this.elements.editCardBtn.addEventListener("click", () => this.renderForm());
        this.elements.self.prepend(this.elements.draggableElement, this.elements.deleteCardBtn, this.elements.deleteCardForm);
        this.elements.self.append(this.elements.moreInfoBtn, this.elements.editCardBtn);
        this.elements.container.append(this.elements.self);
        this.elements.self.classList.add('visits-item');


        this.elements.moreInfoBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.showHideElements(hiddenInfoElements);
            this.elements.id.classList.add('invisible')
        });


        this.elements.deleteCardBtn.addEventListener('click', () => {
            this.deleteCard(this.id, this.password);
        });

    }

    renderForm() {
        new Modal(new EditVisitForm().getElement(this.infoObject)).getElement()
    }

    deleteCard(id, password) {
        this.elements.deleteCardBtn.classList.add('invisible');
        this.elements.deleteCardForm.classList.remove('invisible');
        this.elements.deleteCardSubmBtn.addEventListener('click', (e) => {
            e.preventDefault();
            if (this.elements.deleteCardPass.value === password) {
                fetch(`http://cards.danit.com.ua/cards/${id}`, {
                    headers: {
                        Authorization: `Bearer ${JSON.parse(localStorage.getItem("auth")).token}`
                    },
                    method: "DELETE",
                });
                this.elements.self.remove()
            } else {
                alert('Wrong Password')
            }
        });

        this.elements.deleteCardCancelBtn.addEventListener('click', (e) => {
            e.preventDefault();
            this.elements.deleteCardForm.classList.add('invisible');
            this.elements.deleteCardBtn.classList.remove('invisible')
        })
    }

    showHideElements(elArr) {
        elArr.forEach(e => e.classList.toggle('invisible'))
    }


    dragCard() {
        const card = this.elements.self;
        const container = this.elements.container;
        const draggableElement = this.elements.draggableElement;
        draggableElement.onmousedown = function drag(event) {

            let shiftX = event.clientX - draggableElement.getBoundingClientRect().left;
            let shiftY = draggableElement.getBoundingClientRect().top;

            card.style.position = 'absolute';
            card.style.zIndex = '1000';
            container.append(card);

            moveAt(event.pageX, event.pageY);

            function moveAt(pageX, pageY) {
                card.style.left = pageX - shiftX + 'px';
                card.style.top = pageY - shiftY + 'px';
            }

            function onMouseMove(event) {
                moveAt(event.pageX, event.pageY);
            }

            document.addEventListener('mousemove', onMouseMove);

            draggableElement.onmouseup = function () {
                document.removeEventListener('mousemove', onMouseMove);
                card.onmouseup = null;
                card.style.position = 'static';
                // card.style.zIndex = '3';
                draggableElement.onmouseup = null;
            };

        };

        // draggableElement.ondragstart = function () {
        //     return false;
        // };

    }
}



